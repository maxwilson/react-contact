FROM node

RUN mkdir -p /usr/app/react-contact
# Downloading the dependences with npm command
WORKDIR  "/usr/app/react-contact"

COPY package.json yarn.lock ./

COPY . .

RUN yarn

EXPOSE 3000

CMD [ "yarn", "start"]