import React from 'react';
import './Contact.css';

function Contact() {
  return (
    <div className="App">
      <p>Entre em Contato</p>
      <div>
      <form>
        <label>Nome</label>
        <input type="text" id="fname" name="name" placeholder="Informa seu nome completo.." />

        <label>Email</label>
        <input type="email" id="email" name="email" placeholder="Informe seu e-mail" />

        <label>Mensagem</label>
        <textarea id="msg" name="msg" placeholder="Descreva sua mensagem.."></textarea>
        <input type="submit" value="Enviar" />
      </form>
      </div>
    </div>
  );
}

export default Contact;