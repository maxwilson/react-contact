#!/bin/sh

#------------------
# VARIABLES
#------------------
SERVICE_DIR_BASE=/usr/app/react-contact/src/

#------------------
# MAIN
#------------------

cd $SERVICE_DIR_BASE/
/usr/local/bin/node App.js